## @package Dht
#  Documentação do modulo do Dht
#
# Onde são geradas todas as contantes necessarias para comunicação entra cliente e servidor

##
# \brief Classe que guarda as constantes da Dht
class Dht:

    INITIAL_STATE = 000
    REQUEST = 001
    RESPONSE_NODE = 010
    RESPONSE_ROOT = 011
    RESPONSE_ERROR = 100
    CLIENT_DEAD = 101
    ACK = 110
