## @package Cliente
#  Documentação do modulo do Cliente
#
# Onde todas as informações dos clientes e metodos dos No são declarados

# -*- coding: utf-8 -*-


import socket
from dht import Dht
from pbi import Pbi
import thread
import time
from threading import Thread

K = 2**6  # Tamanho maximo da DHT


##
# \brief Essa é a classe que guarda as infomações do cliente
class Client:

    ## Contrutor da classe seta os valores iniciais
    def __init__(self):
        self.code = Dht.INITIAL_STATE
        self.id = None
        self.ip_port = None
        self.next_client = ClientNeighbor()
        self.previous_client = ClientNeighbor()
        self.n_next_client = ClientNeighbor() # prox do prox
        self.p_previous_client = ClientNeighbor() # anterior do anterior
        self.data = (None, None)  # ARQUIVOS


##
# \brief Essa classe é usada pelo cliente para guardar informações dos seus vizinhos
class ClientNeighbor:

    ## Construtor da classe
    def __init__(self, id_client=None, ip=None, inicio_hash=None, fim_hash=None):
        self.id = id_client
        self.ip_port = ip
        self.data = (inicio_hash, fim_hash)  # ARQUIVOS


##
# \brief Essa classe que gera um Nó na rede e tem todos os metodos necessarios para tratamento dos clientes
class No:

    ip = 'localhost'
    port = 3301
    udp_dest = (ip, port)
    client = Client()
    udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udp.settimeout(15)

    ##
    # \brief Método que verifica a posição correta que o cliente deve ficar
    # @param[in] previous Recebe o ID do cliente anterior ao cliente de referencia
    # @param[in] next_client Recebe o ID do cliente posterior ao cliente de referencia
    def distance(self, previous, next_client):
        id = int(self.client.id)
        if (previous < id) and (id < next_client):
            return True
        elif (previous < id) and next_client == 1:
            return True
        elif next_client == 1 and previous == 1:
            return True
        else:
            return False


    ##
    # \brief Método que passa pelos clientes ate encontrar a posição que o corrente deve ficar ou encontrar a chave que o cliente procura
    # @param[in] current_ip_port Recebe o IP:PORT de um cliente que esta sendo verificado
    # @param[in] key Recebe um numero de chave para encontrar qual cliente guarda sua informação
    def find_and_insert_node(self, current_ip_port, key=None):
        udp = self.udp
        client = self.client
        udp.sendto(str(Pbi.REQUEST_ROOT) + ';' + client.id, current_ip_port)
        message, root_client = udp.recvfrom(1024)  # CODIGO; ID ROOT; ID_NEXT; IP_PORT_NEXT

        message = message.split(';')
        current_id = message[1]
        next_id = message[2]
        ip_port_next = eval(message[3])
        keys = eval(message[4])
        response = None

        if ((self.distance(int(current_id), int(next_id)) is False) and (key is None)) or ((key is not None) and (((keys[0] <= key) and (key <= keys[1])) is False)):
            response = self.find_and_insert_node(ip_port_next, key)
        else:
            if key is not None:
                print "O cliente: ", current_id, " esta guardando essa chave: ", key, " possui as chaves: ", str(keys)
            else:
                print [current_id, current_ip_port, next_id, ip_port_next]
                return [current_id, current_ip_port, next_id, ip_port_next]

        return response


    ##
    # \brief Método que espera uma mensagem e cria uma thread para tratar cada mensagem recebida
    def waiting_clients(self):
        udp = self.udp
        while True:
            message, client_connected = udp.recvfrom(1024)
            thread.start_new_thread(self.waiting_thread, tuple([message, client_connected]))


    ##
    # \brief Thread que trata a mensagem recebida pelo cliente
    # @param[in] message Recebe a mensagem a ser tratada para descobrir o que deve ser feito
    # @param[in] client_connected Recebe o IP:PORT do cliente que enviou a mensagem
    def waiting_thread(self, message, client_connected):
        client = self.client

        udp_thread = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        message = message.split(';')

        if int(message[0]) == Pbi.REQUEST_ROOT:
            udp_thread.sendto(str(Pbi.RESPONSE_ROOT) + ';' + client.id + ';' + client.next_client.id + ';' + str(client.next_client.ip_port) + ';' + str(client.data), client_connected)

        elif int(message[0]) == Pbi.NEIGHBOR:
            if message[1] == "0":
                client.previous_client.__init__(message[2], eval(message[3]))
                print "Anterior", client.previous_client.id, client.p_previous_client.id
                udp_thread.sendto(str(Pbi.ACK)+";"+client.previous_client.id+";"+str(client.previous_client.ip_port), client_connected)
            if message[1] == "1":
                client.next_client.__init__(message[2], eval(message[3]), client.data[0]+((client.data[1]-client.data[0])/2)+1, client.data[1])
                client.data = (client.data[0], int(client.data[0])+int((client.data[1]-client.data[0])/2))
                print client.data, client.next_client.data
                print "Proximo", client.next_client.id, client.n_next_client.id
                udp_thread.sendto(str(Pbi.ACK)+";"+client.next_client.id+";"+str(client.next_client.ip_port) + ";" + str(client.next_client.data), client_connected)

        elif int(message[0]) == Pbi.HELLO_NEIGHBOR_NEXT:
            udp_thread.sendto(str(Pbi.IM_ALIVE_BEHIND) + ';' + client.id + ';' + client.next_client.id + ";" + str(client.next_client.ip_port) + ";" + str(client.data), client_connected)

        elif int(message[0]) == Pbi.HELLO_NEIGHBOR_PREV:
            udp_thread.sendto(str(Pbi.IM_ALIVE_BEYOND) + ';' + client.id + ';' + client.previous_client.id + ";" + str(client.previous_client.ip_port), client_connected)

        thread.exit()


    ##
    # \brief Thread que verifica os vizinhos para avisar em caso de morte de algum deles, tambem trata a atualização de dados dos vizinhos para evitar perda de dados da DHT
    def verify_neighbors(self):
        client = self.client
        udp_thread = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        udp_thread.settimeout(5)

        while True:
            time.sleep(3)
            print "-------------"
            try:
                if client.n_next_client.id is not None:
                    print("Verificando o proximo: " + client.next_client.id + " depois do proximo: " + client.n_next_client.id)
                else:
                    print("Verificando o proximo: " + client.next_client.id + " depois do proximo ainda não foi informado")

                udp_thread.sendto(str(Pbi.HELLO_NEIGHBOR_NEXT) + ';' + client.id, client.next_client.ip_port)
                ack, address = udp_thread.recvfrom(1024)
                print ack
                ack = ack.split(";")
                client.n_next_client.__init__(ack[2], eval(ack[3]))
                client.next_client.data = eval(ack[4])
                print("O proximo esta verificado: " + ack[1] + " atualizando hashtable do proximo: " + ack[4])
            except socket.timeout:
                print "O proximo não foi encontrado na rede: " + client.next_client.id
                udp_thread.sendto(str(Dht.CLIENT_DEAD) + ";" + client.next_client.id, self.udp_dest)
                udp_thread.recv(1025)
                client.data = (client.data[0], client.next_client.data[1])
                client.next_client.__init__(client.n_next_client.id,client.n_next_client.ip_port)
                client.n_next_client.__init__(None, None)
                print "Minha nova hashtable: ", str(client.data)
            try:
                if client.p_previous_client.id is not None:
                    print("Verificando o anterior: " + client.previous_client.id + " antes do anterior: " + client.p_previous_client.id)
                else:
                    print("Verificando o anterior: " + client.previous_client.id + " antes do anterior ainda não foi informado")

                udp_thread.sendto(str(Pbi.HELLO_NEIGHBOR_PREV) + ';' + client.id, client.previous_client.ip_port)
                ack, address = udp_thread.recvfrom(1024)
                print ack
                ack = ack.split(";")
                client.p_previous_client.__init__(ack[2],eval(ack[3]))
                print("O anterior esta verificado: " + ack[1])
            except socket.timeout:
                print "O anterior não foi encontrado na rede: " + client.previous_client.id
                client.previous_client.__init__(client.p_previous_client.id, client.p_previous_client.ip_port)
                client.p_previous_client.__init__(None, None)

    ##
    # \brief Contrutor da classe onde se inicia a comunicação com o servidor e o root
    def __init__(self):
        udp = self.udp
        udp.sendto(str(Dht.REQUEST), self.udp_dest)

        udp_response, udp_server = udp.recvfrom(1024)
        print udp_response

        dht_response = udp_response.split(';')
        udp.sendto(str(Pbi.ACK) + ';' + dht_response[1], udp_server)

        client = self.client
        client.code = int(dht_response[0])
        client.id = dht_response[1]
        client.ip_port = dht_response[3]

        if client.code == Dht.RESPONSE_NODE:
            print "Escolha 1(inserir) ou 2(procurar arquivo): "
            action = raw_input()

            if int(action) == 1:
                insert_between = self.find_and_insert_node(eval(dht_response[2]))
                client.previous_client.__init__(insert_between[0], insert_between[1])
                client.next_client.__init__(insert_between[2], insert_between[3])
                print "anterior", self.client.previous_client.id, "proximo", client.next_client.id

                # Eu sou seu anterior
                udp.sendto(str(Pbi.NEIGHBOR) + ";" + "0;" + self.client.id + ';' + client.ip_port, insert_between[3])
                message, node_new_port = udp.recvfrom(1024)   #TRATAR TIMEOUT

                # Eu sou seu proximo
                udp.sendto(str(Pbi.NEIGHBOR) + ";" + "1;" + client.id + ';' + client.ip_port, insert_between[1])
                message, node_new_port = udp.recvfrom(1024)  #TRATAR TIMEOUT
                message = message.split(';')

                client.data = eval(message[3])
                print "Minha parte da DHT: ", str(client.data)

            elif int(action) == 2:
                print "Nome do arquivo: "
                hash_key = raw_input()
                hash_key = abs(hash(hash_key)) % K
                self.find_and_insert_node(eval(dht_response[2]), hash_key)
                exit()
        else:
            # ROOT
            client.next_client.__init__(client.id, eval(client.ip_port))
            client.n_next_client.__init__(client.id, client.next_client.ip_port)
            client.previous_client.__init__(client.id, eval(client.ip_port))
            client.p_previous_client.__init__(client.id, client.previous_client.ip_port)
            client.data = (1, K) #Tamanho inicial da DHT

        thread_waiting_clients = Thread(None, self.waiting_clients, None)
        thread_waiting_clients.start()
        thread_verify_neighbors = Thread(None, self.verify_neighbors, None)
        thread_verify_neighbors.start()

        thread_waiting_clients.join()

No = No()














