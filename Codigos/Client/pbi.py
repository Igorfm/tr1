## @package Pbi
#  Documentação do modulo do Pbi
#
# Onde são geradas todas as contantes necessarias para uso na comunicação entre os clientes

##
# \brief Classe que guarda as constantes da Pbi
class Pbi:

    REQUEST_ROOT = 000
    RESPONSE_ROOT = 001
    REQUEST = 010
    INSERT = 011
    ACK = 100
    NEXT = 101
    NEIGHBOR = 110
    OK = 111
    LOOP = 1000
    LOOK = 1001
    SAVE = 1010
    HELLO_NEIGHBOR_NEXT = 1011
    HELLO_NEIGHBOR_PREV = 1100
    IM_ALIVE_BEHIND = 1101
    IM_ALIVE_BEYOND = 1110
