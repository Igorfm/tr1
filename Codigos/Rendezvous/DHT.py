## @package DHT
#  Documentação do modulo do DHT
#
# Onde são geradas todas as contantes necessarias para uso na comunicacao entre clientes e servidor

##
# \brief Classe que guarda as constantes da DHT
class DHT:

    INITIAL_STATE = 000
    REQUEST = 001
    RESPONSE_NODE = 010
    RESPONSE_ROOT = 011
    RESPONSE_ERROR = 100
    CLIENT_DEAD = 101
    ACK = 110