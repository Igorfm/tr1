## @package Rendezvous
#  Documentação do modulo do Rendezvous
#
# Onde são guardadas e gerados informações da DHT, como Root e ids dos clientes

# -*- coding: utf-8 -*-

import socket
import thread
import threading
from random import randint
from DHT import DHT


##
# \brief Classe utilizada como conteiner para as estruturas que são utilizadas para geras os IDs
class Rendezvous:

    DHT = []
    reservedIDs = []
    rootAdrress = []
    requestsRunning = []

    IDMAX = 64
    IP = 'localhost'
    PORT = 3301

    lock1 = threading.Lock()

    ##The constructor
    def __init__(self):
        self.socketUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socketUDP.bind((self.IP, self.PORT))

##
# \brief Essa funcão é responsável por gerar o ID do cliente e envia-lo
# @param[in] serverRDZ A referência para a classe Rendezvous que é utilizada como conteiner das estruturas utilizadas
# @param[in] address Endereco do socket do cliente, resposta com ID do cliente na DHT será enviada para esse enderećo
def request(serverRDZ, address):

    #Cliente está pedindo ID novamente e sua thread já foi encerrada, removendo seu ID anterior da lista e gerando um novo
    for i in xrange(len(serverRDZ.DHT) - 1, -1, -1):
        if serverRDZ.DHT[i][1] == int(address[1]):
            del serverRDZ.DHT[i]
            break

    udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    if len(serverRDZ.DHT) == serverRDZ.IDMAX:
        message = str(DHT.RESPONSE_ERROR)
        serverRDZ.socketUDP.sendto(message, address)
        return

    while True:
        if not serverRDZ.DHT:    #Root
            id = 1
            message = str(DHT.RESPONSE_ROOT) + ';' + str(id) + ';' + ';' + str(address)
            udp.sendto(message, address)
            serverRDZ.rootAdrress = address
            serverRDZ.reservedIDs.append(id)

        else:
            serverRDZ.lock1.acquire() #lock
            while True:
                id = randint(2, serverRDZ.IDMAX)

                if not filter(lambda x: x[0] == id, serverRDZ.DHT) and not filter(lambda x: x == id, serverRDZ.reservedIDs):
                    serverRDZ.reservedIDs.append(id)
                    serverRDZ.lock1.release() #unlock
                    break

            message = str(DHT.RESPONSE_NODE) + ';' + str(id) + ';' + str(serverRDZ.rootAdrress) + ';' + str(address)
            udp.sendto(message, address)

        udp.settimeout(5)
        for x in range(0, 2):
            try:
                data, addr = udp.recvfrom(1024)
                msg = data.split(";")
                if len(msg) == 2 and int(msg[1]) == id:
                    break
                else:
                    udp.sendto(message, address)

            except socket.timeout:
                #Timeout resending
                udp.sendto(message, address)

        serverRDZ.lock1.acquire() #lock
        serverRDZ.reservedIDs.remove(id)
        serverRDZ.DHT.append(tuple([int(id), int(address[1])]))
        serverRDZ.DHT.sort(key=lambda dht: dht[0])
        serverRDZ.lock1.release() #unlock
        print serverRDZ.DHT

        break

##
# \brief Essa funcão é responsável por remover o ID da estrutada de referência da DHT
# @param[in] serverRDZ A referência para a classe Rendezvous que é utilizada como conteiner das estruturas utilizadas
# @param[in] id que será removido
# @param[in] address Endereco do socket do cliente, resposta com ID do cliente na DHT será enviada para esse enderećo
def deadClient(serverRDZ, id, addr):
    for i in xrange(len(serverRDZ.DHT) - 1, -1, -1):
        if serverRDZ.DHT[i][0] == int(id):
            del serverRDZ.DHT[i]
            break

    print serverRDZ.DHT

    msg = str(DHT.ACK)
    udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udp.sendto(msg, address)


server = Rendezvous()

while True:
    data, address = server.socketUDP.recvfrom(1024)
    msg = data.split(";")

    if int(msg[0]) == DHT.REQUEST:
        if not server.DHT:
            request(server, address)
        else:
            try:
                if not filter(lambda x: x == int(address[1]), server.requestsRunning):
                    server.requestsRunning.append(int(address[1]))
                    thread.start_new_thread(request, tuple([server, address]))
                else:
                    print "Thread ja iniciada para esse cliente"
            except:
                print "Error: unable to start thread"
    elif int(msg[0]) == DHT.CLIENT_DEAD:
        try:
            thread.start_new_thread(deadClient, tuple([server, msg[1], address]))
        except:
                print "Error: unable to start thread"
